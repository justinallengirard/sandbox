import React from "react";
import "./App.css";
import NavBar from "./components/NavBar.js";
import Body from "./components/Body.js";

function App() {
  return (
    <div className="App">
      <NavBar />
      <Body />
    </div>
  );
}

export default App;
