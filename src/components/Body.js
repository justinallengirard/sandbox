import React from "react";
import Grid from "@material-ui/core/Grid";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";

const useStyles = makeStyles((theme) => ({
  skeleton: {
    height: 200,
    width: "100%",
  },
  container: {
    padding: 24,
  },
}));

export default function Body() {
  const classes = useStyles();

  return (
    <div>
      <Toolbar />
      <div className={classes.container}>
        <Grid container direction="row" spacing={3}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Skeleton variant="rect" className={classes.skeleton} />
          </Grid>
          <Grid item xs={12} sm={6} md={3} lg={3}>
            <Skeleton variant="rect" className={classes.skeleton} />
          </Grid>
          <Grid item xs={12} sm={6} md={3} lg={3}>
            <Skeleton variant="rect" className={classes.skeleton} />
          </Grid>
          <Grid item xs={12} sm={6} md={3} lg={3}>
            <Skeleton variant="rect" className={classes.skeleton} />
          </Grid>
          <Grid item xs={12} sm={6} md={3} lg={3}>
            <Skeleton variant="rect" className={classes.skeleton} />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
