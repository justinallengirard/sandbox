import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "fontsource-roboto";
import * as serviceWorker from "./serviceWorker";
import { StrictMode } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";

ReactDOM.render(
  <StrictMode>
    <CssBaseline>
      <App />
    </CssBaseline>
  </StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
